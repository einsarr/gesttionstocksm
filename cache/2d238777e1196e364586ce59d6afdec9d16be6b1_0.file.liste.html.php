<?php
/* Smarty version 3.1.30, created on 2020-05-25 17:37:27
  from "C:\wamp\www\frameworks\gestionstocksm\src\view\produits\liste.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ecc02572404f5_03103086',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2d238777e1196e364586ce59d6afdec9d16be6b1' => 
    array (
      0 => 'C:\\wamp\\www\\frameworks\\gestionstocksm\\src\\view\\produits\\liste.html',
      1 => 1590427830,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:src/view/header.html' => 1,
    'file:src/view/footer.html' => 1,
  ),
),false)) {
function content_5ecc02572404f5_03103086 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:src/view/header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalProduit">
		<i class="fas fa-plus"></i> Nouveau
	  </button><br>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Liste des produits</h6>
        </div>
        <div class="card-body">
            <div class="text-success" id="message"></div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Libellé</th>
                            <th>Quantité en stock</th>
                            <th>User</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody id="result-produits">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

<div class="modal fade" id="modalProduit">
    <div class="modal-dialog">
        <form method="post" id="produit_form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <div class="modal-body">
                    <h4 class="modal-title text-primary"></h4>
                    <div class="form-group">
                        <label>Libellé</label>
                        <input type="text" name="libelle" id="libelle" class="form-control" placeholder="Libellé du produit" required>
                    </div>
                    <div class="form-group">
                        <label>Quantité</label>
                        <input type="text" name="qteStock" id="qteStock" class="form-control" placeholder="Quantité stocké" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" id="id">
                    <input type="submit" name="action" id="action" class="btn btn-primary" value="Add">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </form>
    </div>
    <!-- /.modal-dialog -->
</div>
<?php $_smarty_tpl->_subTemplateRender("file:src/view/footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
