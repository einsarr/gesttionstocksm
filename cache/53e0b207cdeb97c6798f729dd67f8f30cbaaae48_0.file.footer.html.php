<?php
/* Smarty version 3.1.30, created on 2020-05-25 21:36:20
  from "C:\wamp\www\frameworks\gestionstocksm\src\view\footer.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ecc3a54a75a93_71090471',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '53e0b207cdeb97c6798f729dd67f8f30cbaaae48' => 
    array (
      0 => 'C:\\wamp\\www\\frameworks\\gestionstocksm\\src\\view\\footer.html',
      1 => 1590442135,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ecc3a54a75a93_71090471 (Smarty_Internal_Template $_smarty_tpl) {
?>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Ges-User 2020</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Déconnexion en cours ...</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Voulez-vous quitter ?</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
                <a class="btn btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Login/logout">Quitter</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
public/template/vendor/jquery/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
public/template/vendor/bootstrap/js/bootstrap.bundle.min.js"><?php echo '</script'; ?>
>

<!-- Core plugin JavaScript-->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
public/template/vendor/jquery-easing/jquery.easing.min.js"><?php echo '</script'; ?>
>

<!-- Custom scripts for all pages-->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
public/template/js/sb-admin-2.min.js"><?php echo '</script'; ?>
>

<!-- Page level plugins -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
public/template/vendor/chart.js/Chart.min.js"><?php echo '</script'; ?>
>

<!-- Page level custom scripts -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
public/template/js/demo/chart-area-demo.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
public/template/js/demo/chart-pie-demo.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    $(document).ready(function() {
        $('#message').delay(5000).fadeOut();

        //CRUD PRODUIT
        load_produits();
        $('#produit_form').on('submit', function(event) {
            event.preventDefault();
            if ($('#action').val() == 'Add') {
                $.ajax({
                    url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Produit/add",
                    method: "post",
                    data: $('form').serialize(),
                    dataType: "json",
                    success: function(strMessage) {
                        $('#message').text(strMessage);
                        $('#modalProduit').modal('hide');
                        $('#produit_form')[0].reset();
                        load_produits();
                    }
                });
            }
            if ($('#action').val() == 'Edit') {
                $.ajax({
                    url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Produit/Update",
                    data: $('form').serialize(),
                    type: "POST",
                    dataType: "json",
                    success: function(result) {
                        $('#message').text(result);
                        $('#modalProduit').modal('hide');
                        load_produits();
                    },
                });
            }
        });
        $(document).on('click', '.edit-produit', function() {
            var id = $(this).attr('id');
            $.ajax({
                url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Produit/edit/" + id,
                method: "POST",
                dataType: "json",
                success: function(data) {
                    $('#modalProduit').modal('show');
                    $('#libelle').val(data.libelle);
                    $('#qteStock').val(data.qteStock);
                    $('#id').val(data.id);
                    $('#action').val("Edit");
                }
            });
        });
        $(document).on('click', '.delete-produit', function() {
            var id = $(this).attr("id");
            if (confirm("Etes vous sure de vouloir supprimer ?")) {
                $.ajax({
                    url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Produit/delete/" + id,
                    method: "POST",
                    dataType: "json",
                    success: function(data) {
                        $('#message').text(data);
                        //alert(data);
                        load_produits();
                    }
                });
            }
        });
        //>CRUD PRODUIT>
        //CRUD ENTREE
        load_entrees();
        $('#entree_form').on('submit', function(event) {
            event.preventDefault();
            if ($('#action').val() == 'Add') {
                $.ajax({
                    url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Entree/add",
                    method: "post",
                    data: $('form').serialize(),
                    dataType: "json",
                    success: function(strMessage) {
                        $('#message').text(strMessage);
                        $('#modalEntree').modal('hide');
                        $('#entree_form')[0].reset();
                        load_entrees();
                    },
                    error: function() {
                        $('#message').text(strMessage);
                    }
                });
            }
            if ($('#action').val() == 'Edit') {
                $.ajax({
                    url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Entree/Update",
                    data: $('form').serialize(),
                    type: "POST",
                    dataType: "json",
                    success: function(result) {
                        $('#message').text(result);
                        $('#modalEntree').modal('hide');
                        load_entrees();
                    },
                });
            }
        });
        $(document).on('click', '.edit-entree', function() {
            var id = $(this).attr('id');
            $.ajax({
                url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Entree/edit/" + id,
                method: "POST",
                dataType: "json",
                success: function(data) {
                    $('#modalEntree').modal('show');
                    $('#produit_id').val(data.produit);
                    $('#dateE').val(data.dateE);
                    $('#qteE').val(data.qteE);
                    $('#id').val(data.id);
                    $('#action').val("Edit");
                }
            });
        });
        $(document).on('click', '.delete-entree', function() {
            var id = $(this).attr("id");
            if (confirm("Etes vous sure de vouloir supprimer ?")) {
                $.ajax({
                    url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Entree/delete/" + id,
                    method: "POST",
                    dataType: "json",
                    success: function(data) {
                        $('#message').text(data);
                        load_entrees();
                    }

                });
            }
        });
        //>CRUD ENTREE>
        //CRUD SORTIE
        load_sorties();
        $('#sortie_form').on('submit', function(event) {
            event.preventDefault();
            if ($('#action').val() == 'Add') {
                $.ajax({
                    url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Sortie/add",
                    method: "post",
                    data: $('form').serialize(),
                    dataType: "json",
                    success: function(strMessage) {
                        $('#message').text(strMessage);
                        $('#modalSortie').modal('hide');
                        $('#sortie_form')[0].reset();
                        load_sorties();
                    },
                    error: function() {
                        $('#message').text(strMessage);
                    }
                });
            }
            if ($('#action').val() == 'Edit') {
                $.ajax({
                    url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Sortie/Update",
                    data: $('form').serialize(),
                    type: "POST",
                    dataType: "json",
                    success: function(result) {
                        $('#message').text(result);
                        $('#modalSortie').modal('hide');
                        load_sorties();
                    },
                });
            }
        });
        $(document).on('click', '.edit-sortie', function() {
            var id = $(this).attr('id');
            $.ajax({
                url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Sortie/edit/" + id,
                method: "POST",
                dataType: "json",
                success: function(data) {
                    $('#modalSortie').modal('show');
                    $('#produit_id').val(data.produit);
                    $('#dateS').val(data.dateS);
                    $('#qteS').val(data.qteS);
                    $('#id').val(data.id);
                    $('#action').val("Edit");
                }
            });
        });
        $(document).on('click', '.delete-sortie', function() {
            var id = $(this).attr("id");
            if (confirm("Etes vous sure de vouloir supprimer ?")) {
                $.ajax({
                    url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Sortie/delete/" + id,
                    method: "POST",
                    dataType: "json",
                    success: function(data) {
                        $('#message').text(data);
                        load_sorties();
                    }
                });
            }
        });


    });



    //Chargement des produits dans le tableau
    function load_produits() {
        $.ajax({
            url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Produit/load_produits",
            dataType: "json",
            success: function(result) {
                $('#result-produits').html(result);
            }
        });
    }
    //Chargement des entrees dans le tableau
    function load_entrees() {
        $.ajax({
            url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Entree/load_entrees",
            dataType: "json",
            success: function(result) {
                $('#result-entrees').html(result);
            }
        });
    }
    //Chargement des sorties dans le tableau
    function load_sorties() {
        $.ajax({
            url: "<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
Sortie/load_sorties",
            dataType: "json",
            success: function(result) {
                $('#result-sorties').html(result);
            }
        });
    }
<?php echo '</script'; ?>
>
</body>

</html><?php }
}
