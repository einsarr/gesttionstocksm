<?php
/* Smarty version 3.1.30, created on 2020-05-25 20:09:45
  from "C:\wamp\www\frameworks\gestionstocksm\src\view\entrees\liste.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ecc2609a01b36_38411893',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c96b2ea465cbfad2c460888d69401d71cbf6a257' => 
    array (
      0 => 'C:\\wamp\\www\\frameworks\\gestionstocksm\\src\\view\\entrees\\liste.html',
      1 => 1590437320,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:src/view/header.html' => 1,
    'file:src/view/footer.html' => 1,
  ),
),false)) {
function content_5ecc2609a01b36_38411893 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:src/view/header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalEntree">
		<i class="fas fa-plus"></i> Nouveau
	  </button><br>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Liste des entrées</h6>
        </div>
        <div class="card-body">
            <div class="text-success" id="message"></div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Date entrée</th>
                            <th>Quantité</th>
                            <th>Produit</th>
                            <th>User</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody id="result-entrees">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->


<div class="modal fade" id="modalEntree">
    <div class="modal-dialog">
        <form method="post" id="entree_form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <div class="modal-body">
                    <h4 class="modal-title text-primary"></h4>
                    <div class="form-group">
                        <label>Produit</label>
                        <select class="form-control" name="produit_id" id="produit_id">
                            <option value="">---Choisir l'agence---</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['produits']->value, 'produit');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['produit']->value) {
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['produit']->value->getId();?>
"><?php echo $_smarty_tpl->tpl_vars['produit']->value->getLibelle();?>
 </option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                        </select>
                    </div>
                    <div class="form-group">
                        <label>Date d'entrée</label>
                        <input type="date" name="dateE" id="dateE" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Quantité entrant</label>
                        <input type="text" name="qteE" id="qteE" class="form-control" placeholder="Quantité entrant" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" id="id">
                    <input type="submit" name="action" id="action" class="btn btn-primary" value="Add">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Fermer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </form>
    </div>
    <!-- /.modal-dialog -->
</div>
<?php $_smarty_tpl->_subTemplateRender("file:src/view/footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
