<?php
use libs\system\Controller;
use src\model\ProduitRepository;
use src\model\EntreeRepository;
use src\model\UserRepository;
class EntreeController extends Controller
{
    private $data;
    public function __construct()
    {
        parent::__construct();
        session_start();
        if(isset($_SESSION['user_session'])) {
            $this->data['user'] = $_SESSION['user_session'];
        } else {
            $this->view->redirect('Login');
        }
    }
    public function liste()
    {
        $userdb = new UserRepository();
        $this->data['users'] = $userdb->listeUser();

        $produit = new ProduitRepository();
        $this->data['produits'] = $produit->listeProduits();
       return $this->view->load("entrees/liste",$this->data);
    }

    public function load_entrees()
    {
        $entree = new EntreeRepository();
        $entrees = $entree->listeEntrees();
        $num=0;
        $output="";
        foreach($entrees as $key=>$value)
        {
            $output.= "<tr>
                    <td>".++$num."</td>
                    <td>".$value->getDateE()."</td>
                    <td>".$value->getQteE()."</td>
                    <td>".$value->getProduit()->getLibelle()."</td>
                    <td>".$value->getUser()->getPrenom()." ".$value->getUser()->getNom()."</td>
                    <td>
                        <button type='button' name='edit' id='".$value->getId()."' class='btn btn-warning btn-xs edit-entree'><span class='fa fa-edit'></span></button>
                        <button type='button' name='delete' id='".$value->getId()."' class='btn btn-danger btn-xs delete-entree'><span class='fa fa-trash'></span></button>
                    </td>
                </tr>";
        }
        echo json_encode($output);
    }
    
    public function add(){
        $entree = new EntreeRepository();
        extract($_POST);
        $entreeObject = new Entree();
        $produit = $entree->getProduit($produit_id);
        $entreeObject->setDateE(addslashes($dateE));
        $entreeObject->setQteE(addslashes($qteE));
        $entreeObject->setProduit($produit);
        $entreeObject->setUser($entree->getUser($_SESSION['user_session']));

        $produit->setQteStock($produit->getQteStock()+$qteE);

        $entree->addEntree($entreeObject);
        echo json_encode("ajout réussie avec succès");
   }
   public function edit($id){
        $entree = new EntreeRepository();
        $data = $entree->getEntree($id);
        $output = array(
            'id' => $data->getId(),
            'dateE' => $data->getDateE(),
            'qteE' => $data->getQteE(),
            'produit' => $data->getProduit()->getId(),
        );
        echo json_encode($output);
    }
    public function update(){
        $entree = new EntreeRepository();
        extract($_POST);
        $entOBJT = $entree->getEntree($id);
        $entreeObject = new Entree();
        $entreeObject->setId(addslashes($id));
        $produit = $entree->getProduit($produit_id);
        $entreeObject->setDateE(addslashes($dateE));
        $entreeObject->setProduit($produit);
        $entreeObject->setUser($produit->getUser($_SESSION['user_session']));
        //Enlever la quantité
        $produit->setQteStock($produit->getQteStock()-$entOBJT->getQteE());

        $entreeObject->setQteE(addslashes($qteE));

        $produit->setQteStock($produit->getQteStock()+$qteE);

        $entree->updateEntree($entreeObject);
        echo json_encode("Mise à jour effectué avec succès");
   }
   public function delete($id){
        $entree = new EntreeRepository();
        $entOBJT = $entree->getEntree($id);
        $produit = $entOBJT->getProduit();
        $produit->setQteStock($produit->getQteStock()-$entOBJT->getQteE());
        $entree->deleteEntree($id);
        $message = "Suppression reussie";
        echo json_encode($message);
    }

}