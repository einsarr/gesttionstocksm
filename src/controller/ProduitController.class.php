<?php
use libs\system\Controller;
use src\model\UserRepository;
use src\model\ProduitRepository;
class ProduitController extends Controller
{
    private $data;
    public function __construct()
    {
        parent::__construct();
        session_start();
        if(isset($_SESSION['user_session'])) {
            $this->data['user'] = $_SESSION['user_session'];
        } else {
            $this->view->redirect('Login');
        }
    }
    public function liste()
    {
        $userdb = new UserRepository();
        $this->data['users'] = $userdb->listeUser();
        return $this->view->load("produits/liste",$this->data);
    }

    public function load_produits()
    {
        $produit = new ProduitRepository();
        $produits = $produit->listeProduits();
        $num=0;
        $output="";
        foreach($produits as $key=>$value)
        {
            $output.= "<tr>
                    <td>".++$num."</td>
                    <td>".$value->getLibelle()."</td>
                    <td>".$value->getQteStock()."</td>
                    <td>".$value->getUser()->getPrenom()." ".$value->getUser()->getNom()."</td>
                    <td>
                        <button type='button' name='edit' id='".$value->getId()."' class='btn btn-warning btn-xs edit-produit'><span class='fa fa-edit'></span></button>
                        <button type='button' name='delete' id='".$value->getId()."' class='btn btn-danger btn-xs delete-produit'><span class='fa fa-trash'></span></button>
                    </td>
                </tr>";
        }
        echo json_encode($output);
    }
    public function add(){
        $produit = new ProduitRepository();
        extract($_POST);
        $produitObject = new Produit();
        $produitObject->setLibelle(addslashes($libelle));
        $produitObject->setQteStock(addslashes($qteStock));
        $produitObject->setUser($produit->getUser($_SESSION['user_session']));
        $produit->addProduit($produitObject);
        echo json_encode("produit ajouter avec succès");
   }
   public function edit($id){
        $produit = new ProduitRepository();
        $data = $produit->getProduit($id);
        $output = array(
            'id' => $data->getId(),
            'libelle' => $data->getLibelle(),
            'qteStock' => $data->getQteStock(),
        );
        echo json_encode($output);
    }
    public function update(){
        $produit = new ProduitRepository();
        extract($_POST);
        $produitObject = new Produit();
        $produitObject->setId(addslashes($id));
        $produitObject->setLibelle(addslashes($libelle));
        $produitObject->setQteStock(addslashes($qteStock));
        $produitObject->setUser($produit->getUser($_SESSION['user_session']));
        $produit->updateProduit($produitObject);
        echo json_encode("Mise à jour effectué avec succès");
   }
   public function delete($id){
        $produit = new ProduitRepository();
        $produit->deleteProduit($id);
        $message = "Suppression reussie";
        echo json_encode($message);
    }
}