<?php
use libs\system\Controller;
use src\model\ProduitRepository;
use src\model\SortieRepository;
use src\model\UserRepository;
class SortieController extends Controller
{
    private $data;
    public function __construct()
    {
        parent::__construct();
        session_start();
        if(isset($_SESSION['user_session'])) {
            $this->data['user'] = $_SESSION['user_session'];
        } else {
            $this->view->redirect('Login');
        }
    }
    public function liste()
    {
        $userdb = new UserRepository();
        $this->data['users'] = $userdb->listeUser();

        $produit = new ProduitRepository();
        $this->data['produits'] = $produit->listeProduits();
       return $this->view->load("sorties/liste",$this->data);
    }
    
    public function load_sorties()
    {
        $sortie = new SortieRepository();
        $sorties = $sortie->listeSorties();
        $num=0;
        $output="";
        foreach($sorties as $key=>$value)
        {
            $output.= "<tr>
                    <td>".++$num."</td>
                    <td>".$value->getDateS()."</td>
                    <td>".$value->getQteS()."</td>
                    <td>".$value->getProduit()->getLibelle()."</td>
                    <td>".$value->getUser()->getPrenom()." ".$value->getUser()->getNom()."</td>
                    <td>
                        <button type='button' name='edit' id='".$value->getId()."' class='btn btn-warning btn-xs edit-sortie'><span class='fa fa-edit'></span></button>
                        <button type='button' name='delete' id='".$value->getId()."' class='btn btn-danger btn-xs delete-sortie'><span class='fa fa-trash'></span></button>
                    </td>
                </tr>";
        }
        echo json_encode($output);
    }
    public function add(){
        $message='';
        $sortie = new SortieRepository();
        extract($_POST);
        $sortieObject = new Sortie();
        $produit = $sortie->getProduit($produit_id);
        $sortieObject->setDateS(addslashes($dateS));
        $sortieObject->setQteS(addslashes($qteS));
        $sortieObject->setProduit($produit);
        $sortieObject->setUser($sortie->getUser($_SESSION['user_session']));

        if($produit->getQteStock() < $qteS){
            $message = "Stock insuffisant";
        }
        else{
            $produit->setQteStock($produit->getQteStock()-$qteS);
            $sortie->addSortie($sortieObject);
            $message="ajout réussie avec succès";
        }
        echo json_encode($message);
   }
   public function edit($id){
        $sortie = new SortieRepository();
        $data = $sortie->getSortie($id);
        $output = array(
            'id' => $data->getId(),
            'dateS' => $data->getDateS(),
            'qteS' => $data->getQteS(),
            'produit' => $data->getProduit()->getId(),
        );
        echo json_encode($output);
    }
    public function update(){
        $sortie = new SortieRepository();
        extract($_POST);
        $sortOBJT = $sortie->getSortie($id);
        $sortieObject = new Sortie();
        $sortieObject->setId(addslashes($id));
        $produit = $sortie->getProduit($produit_id);
        $sortieObject->setDateS(addslashes($dateS));
        $sortieObject->setProduit($produit);
        $sortieObject->setUser($produit->getUser($_SESSION['user_session']));
        //Enlever la quantité
        $produit->setQteStock($produit->getQteStock()+$sortOBJT->getQteS());

        $sortieObject->setQteS(addslashes($qteS));

        $produit->setQteStock($produit->getQteStock()-$qteS);

        $sortie->updateSortie($sortieObject);
        echo json_encode("Mise à jour effectué avec succès");
   }
   public function delete($id){
        $sortie = new SortieRepository();
        $sortOBJT = $sortie->getSortie($id);
        $produit = $sortOBJT->getProduit();
        $produit->setQteStock($produit->getQteStock()+$sortOBJT->getQteS());
        $sortie->deleteSortie($id);
        $message = "Suppression reussie";
        echo json_encode($message);
    }
}