<?php
use Doctrine\ORM\Annotation as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="entree")
 **/
class Entree
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    private $id;
    /** @Column(type="string") **/
    private $dateE;
    /** @Column(type="integer") **/
    private $qteE;

     /**
     * Many Entree have one Produit. This is the owning side.
     * @ManyToOne(targetEntity="Produit", inversedBy="entrees")
     * @JoinColumn(name="produit_id", referencedColumnName="id")
     */
    private $produit;
    /**
     * Many Entree have one User. This is the owning side.
     * @ManyToOne(targetEntity="User", inversedBy="entrees")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    public function __construct()
    {
    }
    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getDateE()
    {
        return $this->dateE;
    }
    public function setDateE($dateE)
    {
        $this->dateE = $dateE;
    }

    public function getQteE()
    {
        return $this->qteE;
    }
    public function setQteE($qteE)
    {
        $this->qteE = $qteE;
    }

    public function getProduit()
    {
        return $this->produit;
    }
    public function setProduit($produit)
    {
        $this->produit = $produit;
    }
    public function getUser()
    {
        return $this->user;
    }
    public function setUser($user)
    {
        $this->user = $user;
    }
    
}

?>
