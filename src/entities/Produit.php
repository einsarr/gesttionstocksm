<?php
use Doctrine\ORM\Annotation as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="produit")
 **/
class Produit
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    private $id;
    /** @Column(type="string") **/
    private $libelle;
    /** @Column(type="integer") **/
    private $qteStock;
    /**
     * One Produit has many Entrees. This is the inverse side.
     * @OneToMany(targetEntity="Entree", mappedBy="Produit")
     */
    private $entrees;
     /**
     * One Produit has many Sorties. This is the inverse side.
     * @OneToMany(targetEntity="Sortie", mappedBy="Produit")
     */
    private $sorties;
    /**
     * Many Produit have one User. This is the owning side.
     * @ManyToOne(targetEntity="User", inversedBy="produits")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    public function __construct()
    {
        $this->entrees = new ArrayCollection();
        $this->sorties = new ArrayCollection();
    }
    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getLibelle()
    {
        return $this->libelle;
    }
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    public function getQteStock()
    {
        return $this->qteStock;
    }
    public function setQteStock($qteStock)
    {
        $this->qteStock = $qteStock;
    }

    public function getEntrees()
    {
        return $this->entrees;
    }
    public function setEntrees($entrees)
    {
        $this->entrees = $entrees;
    }

    public function getSorties()
    {
        return $this->sorties;
    }
    public function setSorties($sorties)
    {
        $this->sorties = $sorties;
    }
    public function getUser()
    {
        return $this->user;
    }
    public function setUser($user)
    {
        $this->user = $user;
    }

    
}

?>
