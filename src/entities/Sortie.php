<?php
use Doctrine\ORM\Annotation as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="sortie")
 **/
class Sortie
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    private $id;
    /** @Column(type="string") **/
    private $dateS;
    /** @Column(type="integer") **/
    private $qteS;
    /**
     * Many Sortie have one Produit. This is the owning side.
     * @ManyToOne(targetEntity="Produit", inversedBy="sorties")
     * @JoinColumn(name="produit_id", referencedColumnName="id")
     */
    private $produit;
    /**
     * Many Sortie have one User. This is the owning side.
     * @ManyToOne(targetEntity="User", inversedBy="sorties")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    public function __construct()
    {
        
    }
    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getDateS()
    {
        return $this->dateS;
    }
    public function setDateS($dateS)
    {
        $this->dateS = $dateS;
    }

    public function getProduit()
    {
        return $this->produit;
    }
    public function setProduit($produit)
    {
        $this->produit = $produit;
    }
    public function getUser()
    {
        return $this->user;
    }
    public function setUser($user)
    {
        $this->user = $user;
    }
    public function getQteS()
    {
        return $this->qteS;
    }
    public function setQteS($qteS)
    {
        $this->qteS = $qteS;
    }

}

?>
