<?php
use Doctrine\ORM\Annotation as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="user")
 **/
class User
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    private $id;
    /** @Column(type="string") **/
    private $nom;
    /** @Column(type="string") **/
    private $prenom;
    /** @Column(type="string") **/
    private $email;
    /** @Column(type="string") **/
    private $password;
    /**
     * Many Users have Many Roles.
     * @ManyToMany(targetEntity="Roles", inversedBy="users")
     * @JoinTable(name="users_roles")
     */
    private $roles;
     /**
     * One User has many Produits. This is the inverse side.
     * @OneToMany(targetEntity="Produit", mappedBy="User")
     */
    private $produits;
     /**
     * One User has many Entrees. This is the inverse side.
     * @OneToMany(targetEntity="Entree", mappedBy="User")
     */
    private $entrees;
     /**
     * One User has many Sorties. This is the inverse side.
     * @OneToMany(targetEntity="Sortie", mappedBy="User")
     */
    private $sorties;
    
    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->produits = new ArrayCollection();
        $this->entrees = new ArrayCollection();
        $this->sorties = new ArrayCollection();
    }
    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNom()
    {
        return $this->nom;
    }
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    public function getPassword()
    {
        return $this->password;
    }
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getRoles()
    {
        return $this->roles;
    }
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }
    public function getProduits()
    {
        return $this->produits;
    }
    public function setProduits($produits)
    {
        $this->produits = $produits;
    }
    public function getEntrees()
    {
        return $this->entrees;
    }
    public function setEntrees($entrees)
    {
        $this->entrees = $entrees;
    }
    public function getSorties()
    {
        return $this->sorties;
    }
    public function setSorties($sorties)
    {
        $this->sorties = $sorties;
    }

    public function hasRole($nom)
    {
        $bol = 0;
        foreach ($this->roles as $role)
        {
            if($role->getNom() == $nom)
            {
                $bol = 1;
            }
        }
        return $bol;
    }
}

?>
