<?php
/*==================================================
MODELE MVC DEVELOPPE PAR Ngor SECK
ngorsecka@gmail.com
(+221) 77 - 433 - 97 - 16
PERFECTIONNEZ CE MODELE ET FAITES MOI UN RETOUR
POUR TOUTE MODIFICATION VISANT A L'AMELIORER.
VOUS ETES LIBRE DE TOUTE UTILISATION.
===================================================*/
namespace src\model; 

use libs\system\Model; 
	
class EntreeRepository extends Model{
	
	/**
	 * Methods with DQL (Doctrine Query Language) 
	 */
	public function __construct(){
		parent::__construct();
    }
    public function getEntree($id)
	{
		if($this->db != null)
		{
			return $this->db->getRepository('Entree')->find(array('id' => $id));
		}
	}
	public function getProduit($id)
	{
		if($this->db != null)
		{
			return $this->db->getRepository('Produit')->find(array('id' => $id));
		}
    }
    public function getUser($id)
	{
		if($this->db != null)
		{
			return $this->db->getRepository('User')->find(array('id' => $id));
		}
	}
	
	public function addEntree($Entree)
	{
		if($this->db != null)
		{
			$this->db->persist($Entree);
			$this->db->flush();

			return $Entree->getId();
		}
	}
	
	public function deleteEntree($id){
		if($this->db != null)
		{
			$Entree = $this->db->find('Entree', $id);
			if($Entree != null)
			{
				$this->db->remove($Entree);
				$this->db->flush();
			}else {
				die("Objet ".$id." does not existe!");
			}
		}
	}
	
	public function updateEntree($Entree){
		if($this->db != null)
		{
			$getEntree = $this->db->find('Entree', $Entree->getId());
			if($getEntree != null)
			{
				$getEntree->setDateE($Entree->getDateE());
				$getEntree->setQteE($Entree->getQteE());
				$this->db->flush();

			}else {
				die("Objet ".$Entree->getId()." does not existe!!");
			}	
		}
	}
	
	public function listeEntrees(){
		if($this->db != null)
		{
			return $this->db->createQuery("SELECT e FROM Entree e")->getResult();
		}
	}
}