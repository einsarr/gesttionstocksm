<?php
/*==================================================
MODELE MVC DEVELOPPE PAR Ngor SECK
ngorsecka@gmail.com
(+221) 77 - 433 - 97 - 16
PERFECTIONNEZ CE MODELE ET FAITES MOI UN RETOUR
POUR TOUTE MODIFICATION VISANT A L'AMELIORER.
VOUS ETES LIBRE DE TOUTE UTILISATION.
===================================================*/
namespace src\model; 

use libs\system\Model; 
	
class ProduitRepository extends Model{
	
	/**
	 * Methods with DQL (Doctrine Query Language) 
	 */
	public function __construct(){
		parent::__construct();
    }
    public function getProduit($id)
	{
		if($this->db != null)
		{
			return $this->db->getRepository('Produit')->find(array('id' => $id));
		}
	}
	
	public function addProduit($Produit)
	{
		if($this->db != null)
		{
			$this->db->persist($Produit);
			$this->db->flush();
			return $Produit->getId();
		}
	}
	
	public function deleteProduit($id){
		if($this->db != null)
		{
			$Produit = $this->db->find('Produit', $id);
			if($Produit != null)
			{
				$this->db->remove($Produit);
				$this->db->flush();
			}else {
				die("Objet ".$id." does not existe!");
			}
		}
	}
	
	public function updateProduit($Produit){
		if($this->db != null)
		{
			$getProduit = $this->db->find('Produit', $Produit->getId());
			if($getProduit != null)
			{
                $getProduit->setLibelle($Produit->getLibelle());
                $getProduit->setQteStock($Produit->getQteStock());
				$this->db->flush();
			}else {
				die("Objet ".$Produit->getId()." does not existe!!");
			}	
		}
	}
	
	public function listeProduits(){
		if($this->db != null)
		{
			return $this->db->createQuery("SELECT p FROM Produit p")->getResult();
		}
	}
	public function getUser($id)
	{
		if($this->db != null)
		{
			return $this->db->getRepository('User')->find(array('id' => $id));
		}
	}
}