<?php
/*==================================================
MODELE MVC DEVELOPPE PAR Ngor SECK
ngorsecka@gmail.com
(+221) 77 - 433 - 97 - 16
PERFECTIONNEZ CE MODELE ET FAITES MOI UN RETOUR
POUR TOUTE MODIFICATION VISANT A L'AMELIORER.
VOUS ETES LIBRE DE TOUTE UTILISATION.
===================================================*/
namespace src\model; 

use libs\system\Model; 
	
class SortieRepository extends Model{
	
	/**
	 * Methods with DQL (Doctrine Query Language) 
	 */
	public function __construct(){
		parent::__construct();
    }
    public function getSortie($id)
	{
		if($this->db != null)
		{
			return $this->db->getRepository('Sortie')->find(array('id' => $id));
		}
	}
	public function getProduit($id)
	{
		if($this->db != null)
		{
			return $this->db->getRepository('Produit')->find(array('id' => $id));
		}
    }
    public function getUser($id)
	{
		if($this->db != null)
		{
			return $this->db->getRepository('User')->find(array('id' => $id));
		}
	}
	
	public function addSortie($Sortie)
	{
		if($this->db != null)
		{
			$this->db->persist($Sortie);
			$this->db->flush();

			return $Sortie->getId();
		}
	}
	
	public function deleteSortie($id){
		if($this->db != null)
		{
			$Sortie = $this->db->find('Sortie', $id);
			if($Sortie != null)
			{
				$this->db->remove($Sortie);
				$this->db->flush();
			}else {
				die("Objet ".$id." does not existe!");
			}
		}
	}
	
	public function updateSortie($sortie){
		if($this->db != null)
		{
			$getSortie = $this->db->find('Sortie', $sortie->getId());
			if($getSortie != null)
			{
				$getSortie->setDateS($sortie->getDateS());
				$getSortie->setQteS($sortie->getQteS());
				$this->db->flush();

			}else {
				die("Objet ".$sortie->getId()." does not existe!!");
			}	
		}
	}
	
	public function listeSorties(){
		if($this->db != null)
		{
			return $this->db->createQuery("SELECT s FROM Sortie s")->getResult();
		}
	}
}